PROGRAM main

  ! Addes into .h5m file a nodel coordiates that are of memory array type

  USE HDF5
  USE H5LT
  USE ISO_C_BINDING

  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = KIND(1.d0)
  INTEGER, PARAMETER :: i8 = SELECTED_INT_KIND(10)

  CHARACTER(LEN=180) :: filename

  INTEGER(HID_T) :: file, file1, memtype, dset_id,dspace, filespace, memspace, dspace_g, dset_g ! Handles
  INTEGER(HID_T) :: dataspace_g, datatype_id
  INTEGER :: hdferr
  INTEGER(HSIZE_T), DIMENSION(1:1)   :: dims
  INTEGER(HSIZE_T), DIMENSION(1:1)   :: adims
  INTEGER :: i, j, k
  TYPE(C_PTR) :: f_ptr
  
  INTEGER(HSIZE_T), DIMENSION(:), ALLOCATABLE, TARGET :: coord
  INTEGER :: AllocateStatus
  INTEGER(HSIZE_T), DIMENSION(1:2) :: dims1, maxdims1
  REAL(C_DOUBLE), ALLOCATABLE, DIMENSION(:,:), TARGET :: ncoord
  INTEGER(C_INT64_T), ALLOCATABLE, DIMENSION(:,:), TARGET :: lm
  INTEGER icnt
  LOGICAL :: path_valid
  CHARACTER(LEN=180) :: ElemChar


  CHARACTER(LEN=10), DIMENSION(1:19), PARAMETER :: &
       nprocs = [character(LEN=10) :: '1','2','4','8','16','32','64','128','256','512','1024','2048','4096',&
       '8192','16384','32768','65536','131072','262144']

  !PRINT*,'Enter .h5m file to read/write'
  !READ*,filename

  !
  ! Initialize FORTRAN interface.
  !
  CALL h5open_f(hdferr)

  DO icnt = 1, 1
  !filename = '64bricks_32mtet_ng_rib_'//TRIM(nprocs(icnt))//'.h5m'
  !filename = '64bricks_8mtet_ng_rib_'//TRIM(nprocs(icnt))//'.h5m'
   filename = 'recdia_2.h5m'
  ! filename = 'nuscale_rib_'//TRIM(nprocs(icnt))//'.h5m'
  ! filename = 'rec_sm_R_'//TRIM(ADJUSTL(nprocs(icnt)))//'.h5m'

   CALL h5fopen_f(filename, H5F_ACC_RDWR_F, file, hdferr)
   IF(hdferr.NE.0) CYCLE  


   PRINT*,'Processing ',TRIM(filename)

  CALL h5ltpath_valid_f(file, "/tstt/nodes/COORDINATES", .TRUE., path_valid, hdferr)
  IF(path_valid) GOTO 10

  CALL h5dopen_f(file, "/tstt/nodes/coordinates", dset_g, hdferr)
  CALL h5dget_space_f(dset_g, dataspace_g, hdferr)
  
  CALL h5sget_simple_extent_dims_f(dataspace_g, dims1, maxdims1, hdferr)

  adims = (/3/)
  ALLOCATE(ncoord(1:dims1(1),1:dims1(2)), STAT=AllocateStatus)
  IF(AllocateStatus.NE.0) PRINT*,' ncoord, *** Not enough memory ***'

  f_ptr = C_LOC(ncoord(1,1))

  CALL h5dget_type_f(dset_g, datatype_id, hdferr) 

  CALL h5dread_f(dset_g, datatype_id, f_ptr, hdferr)

  CALL h5dclose_f(dset_g, hdferr)
  CALL h5sclose_f(dataspace_g, hdferr)

  !
  ! Create array datatypes for file and memory.
  !
  CALL H5Tarray_create_f(datatype_id, 1, adims, memtype, hdferr)
  !
  ! Create the dataspace for the dataset
  !
  
  CALL h5screate_simple_f(1, dims1(2), filespace, hdferr)
  !
  ! Create the dataset with default properties and close filespace.
  !
  CALL H5Dcreate_f(file, "/tstt/nodes/COORDINATES", memtype, filespace, dset_id, hdferr)
  CALL H5Sclose_f(filespace, hdferr)
  !
  ! Create the dataset and write the array data to it.
  !
  CALL h5screate_simple_f(1, dims1(2), memspace, hdferr)

  CALL H5Dget_space_f(dset_id, filespace, hdferr)

  f_ptr = C_LOC(ncoord(1,1))
  CALL h5dwrite_f(dset_id, memtype, f_ptr, hdferr, &
       mem_space_id=memspace, file_space_id=filespace)

  CALL H5Dclose_f(dset_id, hdferr)
  CALL H5Sclose_f(memspace, hdferr)
  CALL H5Sclose_f(filespace, hdferr)
  CALL h5tclose_f(datatype_id, hdferr)

  DEALLOCATE(ncoord)

10 CONTINUE

  CALL h5ltpath_valid_f(file, "/tstt/elements/Tet4/CONNECTIVITY", .TRUE., path_valid, hdferr)
  IF(path_valid) GOTO 20
  CALL h5ltpath_valid_f(file, "/tstt/elements/Hex27/CONNECTIVITY", .TRUE., path_valid, hdferr)
  IF(path_valid) GOTO 20

! ******************************
!  CALL h5dopen_f(file, "/tstt/elements/Tri6/connectivity", dset_g, hdferr)

  CALL h5ltpath_valid_f(file, "/tstt/elements/Tet4/connectivity", .TRUE., path_valid, hdferr)
  IF(path_valid)THEN
     ElemChar = "Tet4"
     CALL h5dopen_f(file, "/tstt/elements/"//TRIM(ElemChar)//"/connectivity", dset_g, hdferr)
  ELSE
     ElemChar = "Hex27"
     CALL h5ltpath_valid_f(file, "/tstt/elements/"//TRIM(ElemChar)//"/connectivity", .TRUE., path_valid, hdferr)
     CALL h5dopen_f(file, "/tstt/elements/"//TRIM(ElemChar)//"/connectivity", dset_g, hdferr)
  ENDIF
  CALL h5dget_space_f(dset_g, dataspace_g, hdferr)
  
  CALL h5sget_simple_extent_dims_f(dataspace_g, dims1, maxdims1, hdferr)

  ALLOCATE(lm(1:dims1(1),1:dims1(2)), STAT=AllocateStatus)
  IF(AllocateStatus.NE.0) PRINT*,' lm, *** Not enough memory ***'
  CALL h5dget_type_f(dset_g, datatype_id, hdferr) 
  f_ptr = C_LOC(lm(1,1))

  CALL h5dread_f(dset_g, datatype_id, f_ptr, hdferr)

  CALL h5dclose_f(dset_g, hdferr)
  CALL h5sclose_f(dataspace_g, hdferr)
  
  !
  ! Create array datatypes for file and memory.
  !
  adims = (/dims1(1)/)
  CALL H5Tarray_create_f(datatype_id, 1, adims, memtype, hdferr)
  !
  ! Create the dataspace for the dataset
  !
  
  CALL h5screate_simple_f(1, dims1(2), filespace, hdferr)
  !
  ! Create the dataset with default properties and close filespace.
  !
!  CALL H5Dcreate_f(file, "/tstt/elements/Tri6/CONNECTIVITY", memtype, filespace, dset_id, hdferr)
  CALL H5Dcreate_f(file, "/tstt/elements/"//TRIM(ElemChar)//"/CONNECTIVITY", memtype, filespace, dset_id, hdferr)
  CALL H5Sclose_f(filespace, hdferr)
  !
  ! Create the dataset and write the array data to it.
  !
  CALL h5screate_simple_f(1, dims1(2), memspace, hdferr)

  CALL H5Dget_space_f(dset_id, filespace, hdferr)

  f_ptr = C_LOC(lm(1,1))
  CALL h5dwrite_f(dset_id, memtype, f_ptr, hdferr, &
       mem_space_id=memspace, file_space_id=filespace)

  CALL H5Dclose_f(dset_id, hdferr)
  CALL H5Sclose_f(memspace, hdferr)
  CALL H5Sclose_f(filespace, hdferr)
  CALL h5tclose_f(datatype_id, hdferr)

  DEALLOCATE(lm)

!*********
20 CONTINUE
  ElemChar = "Tri3"
  CALL h5ltpath_valid_f(file, "/tstt/elements/"//TRIM(ElemChar)//"/CONNECTIVITY", .TRUE., path_valid, hdferr)
  IF(path_valid) GOTO 30

  CALL h5ltpath_valid_f(file,  "/tstt/elements/"//TRIM(ElemChar)//"/connectivity", .TRUE., path_valid, hdferr)
  IF(.NOT.path_valid) GOTO 30

  CALL h5dopen_f(file, "/tstt/elements/"//TRIM(ElemChar)//"/connectivity", dset_g, hdferr)
  CALL h5dget_space_f(dset_g, dataspace_g, hdferr)
  
  CALL h5sget_simple_extent_dims_f(dataspace_g, dims1, maxdims1, hdferr)

  ALLOCATE(lm(1:dims1(1),1:dims1(2)), STAT=AllocateStatus)
  IF(AllocateStatus.NE.0) PRINT*,' lm, *** Not enough memory ***'
  CALL h5dget_type_f(dset_g, datatype_id, hdferr) 
  f_ptr = C_LOC(lm(1,1))

  CALL h5dread_f(dset_g, datatype_id, f_ptr, hdferr)

  CALL h5dclose_f(dset_g, hdferr)
  CALL h5sclose_f(dataspace_g, hdferr)
  
  !
  ! Create array datatypes for file and memory.
  !
  adims = (/dims1(1)/)
  CALL H5Tarray_create_f(datatype_id, 1, adims, memtype, hdferr)
  !
  ! Create the dataspace for the dataset
  !
  
  CALL h5screate_simple_f(1, dims1(2), filespace, hdferr)
  !
  ! Create the dataset with default properties and close filespace.
  !
  CALL H5Dcreate_f(file, "/tstt/elements/"//TRIM(ElemChar)//"/CONNECTIVITY", memtype, filespace, dset_id, hdferr)
  CALL H5Sclose_f(filespace, hdferr)
  !
  ! Create the dataset and write the array data to it.
  !
  CALL h5screate_simple_f(1, dims1(2), memspace, hdferr)

  CALL H5Dget_space_f(dset_id, filespace, hdferr)

  f_ptr = C_LOC(lm(1,1))
  CALL h5dwrite_f(dset_id, memtype, f_ptr, hdferr, &
       mem_space_id=memspace, file_space_id=filespace)

  CALL H5Dclose_f(dset_id, hdferr)
  CALL H5Sclose_f(memspace, hdferr)
  CALL H5Sclose_f(filespace, hdferr)
  CALL h5tclose_f(datatype_id, hdferr)

  DEALLOCATE(lm)

!*********

30 CONTINUE

  CALL H5Fclose_f(file, hdferr)

ENDDO

END PROGRAM main
