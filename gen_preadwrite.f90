MODULE qsort_c_module

  USE HDF5

  IMPLICIT NONE
  
CONTAINS

  ! Randomly shuffles an array
  
  SUBROUTINE Shuffle(a)
    INTEGER(HSIZE_T), INTENT(inout) :: a(:)
    INTEGER :: i, randpos
    INTEGER :: temp
    REAL :: r

    DO i = SIZE(a), 2, -1
       CALL RANDOM_NUMBER(r)
       randpos = INT(r * i) + 1
       temp = a(randpos)
       a(randpos) = a(i)
       a(i) = temp
    END DO

  END SUBROUTINE Shuffle

END MODULE qsort_c_module

!************************************************************
!
!  This program generates the random partitioning of a
!  finite element connectivity table.
!
!************************************************************

PROGRAM main

  USE HDF5
  USE H5LT
  USE ISO_C_BINDING
  USE qsort_c_module
  
  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = KIND(1.d0)
  INTEGER, PARAMETER :: i8 = SELECTED_INT_KIND(10)

  CHARACTER(LEN=12), PARAMETER :: dataset   = "partition" ! partitioning

! Number of elements, uncomment the size wanted
INTEGER(KIND=i8), PARAMETER :: dim0 = 50331648
!  INTEGER(KIND=i8), PARAMETER :: dim0 = 805306368_i8
!  INTEGER(KIND=i8), PARAMETER :: dim0 = 1610612736_i8
!  INTEGER(KIND=i8), PARAMETER :: dim0 = 3221225472_i8
!  INTEGER(KIND=i8), PARAMETER :: dim0 = 6442450944_i8

  INTEGER(HID_T)  :: file, dset_id, space ! Handles
  INTEGER :: hdferr
  INTEGER(KIND=i8) :: i
  TYPE(C_PTR) :: f_ptr
  INTEGER(HSIZE_T), DIMENSION(:), ALLOCATABLE, TARGET :: coord_ran
  INTEGER ::  AllocateStatus
  CHARACTER(LEN=12) :: ich5
  INTEGER(HSIZE_T), DIMENSION(1:1) :: dims
  INTEGER(HID_T) :: h5_kind_type
  CHARACTER(LEN=26) :: filename

  !
  ! Initialize FORTRAN predefined datatypes.
  !
  CALL h5open_f(hdferr)

  WRITE(ich5,'(I12.12)') dim0
  filename = 'gen_pbench_'//ich5//'.h5'
  CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file, hdferr)

  ! initialize the data

  dims(1) = dim0

  CALL RANDOM_SEED()
  WRITE(*,'(/A,f14.3,A/)')'Memory Req. = ', REAL(dim0*sizeof(coord_ran(1)))/1073741824.,' Gib'
  ALLOCATE(coord_ran(1:dim0), STAT=AllocateStatus)
  IF(AllocateStatus.NE.0) STOP ' *** Not enough memory ***'

  DO i = 1, dim0
     coord_ran(i) = i
  ENDDO
  PRINT*,'...Started Shuffle'
  CALL Shuffle(coord_ran)
  PRINT*,'...Finished Shuffle'

  h5_kind_type = h5kind_to_type(hsize_t, H5_INTEGER_KIND)

  CALL h5screate_simple_f(1, dims, space, hdferr)

  PRINT*,'... h5dcreate_f: partition dataset'
  CALL h5dcreate_f(file, "partition", h5_kind_type, space, dset_id, hdferr)

  PRINT*,'...Started Writing dataset'
  f_ptr = C_LOC(coord_ran(1))
  CALL h5dwrite_f(dset_id, h5_kind_type, f_ptr, hdferr)
  PRINT*,'.....Finished Write dataset'

  CALL h5sclose_f(space,hdferr)
  CALL h5dclose_f(dset_id,hdferr)
  CALL h5fclose_f(file,hdferr)

END PROGRAM main
