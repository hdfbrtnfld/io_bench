!mpixlf90 -O3 -o pread pread_mpiio.f90
PROGRAM ParallelWrite

  USE ISO_C_BINDING ! Enable Fortran 2003 C bindings
  USE MPI

  IMPLICIT NONE
  INTEGER, PARAMETER :: dp = KIND(1.d0)
  CHARACTER(LEN=180) :: filename

  INTEGER, ALLOCATABLE, DIMENSION(:) :: epart
  LOGICAL, ALLOCATABLE, DIMENSION(:) :: mynodes

  INTEGER :: nn,ne,nnpe, myndtot

  INTEGER :: error  ! Error flags
  INTEGER :: i, j, k ,iaux, nnmax,nnmin, nnavg

  CHARACTER(LEN=5) :: ich5
  !
  ! MPI definitions and calls.
  !
  INTEGER :: mpierror       ! MPI error flag
  INTEGER :: comm, info
  INTEGER :: mpi_size, mpi_rank
  REAL*8 t0, t1, t2, t3, twrite, twritep, twritemin, twritemax, twrite1, twrite2
  REAL*8, DIMENSION(1:3) :: xtiming, timing, timingMin, timingMax

  TYPE(C_PTR) :: f_ptr
  LOGICAL, PARAMETER :: debug = .FALSE.

  REAL :: start, gettime

  ! mpi io start

  INTEGER, PARAMETER :: nelem = 1
  INTEGER :: numpr, myid,  ierr, size, nwrite_particles, iaux2
  INTEGER :: fh
  INTEGER, DIMENSION(1:MPI_STATUS_SIZE) :: status
  INTEGER, ALLOCATABLE, DIMENSION(:) :: disps
  INTEGER, ALLOCATABLE, DIMENSION(:) :: blocklens
  CHARACTER(LEN=180) :: prefix
  INTEGER(kind=MPI_ADDRESS_KIND), DIMENSION(1:nelem) :: offsets

  TYPE Particle
     SEQUENCE
     REAL*8 :: x, y, z
  END TYPE Particle

  TYPE(Particle), ALLOCATABLE, DIMENSION(:) ::  write_particles
  TYPE(Particle), ALLOCATABLE, DIMENSION(:) ::  read_particles
  INTEGER, DIMENSION(1:nelem) :: realtypes, blockcounts
  INTEGER :: particletype, particletype_indx
  CHARACTER(len=MPI_MAX_ERROR_STRING) :: error_string
  INTEGER :: reslen

  ! mpi io end

  CALL MPI_INIT(ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD, numpr, ierr)
  CALL MPI_INFO_CREATE(info, ierr)

  prefix = 'recdia'
  WRITE(ich5,'(I5.5)') numpr

  nwrite_particles = 33554432/numpr

  ALLOCATE(disps(1:nwrite_particles))
  ALLOCATE(blocklens(1:nwrite_particles))
  blocklens = 1
  DO i = 1, nwrite_particles
     disps(i) = myid*nwrite_particles+i
  ENDDO

  offsets(1:nelem) = 0
  realtypes(1:nelem) = MPI_REAL8
  blockcounts(1:nelem) = 3
  CALL MPI_BARRIER(MPI_COMM_WORLD, ierr)
  t0 = MPI_Wtime()

  CALL MPI_Type_create_struct(nelem, blockcounts, offsets, realtypes, particletype, ierr)
  IF(ierr.NE.MPI_SUCCESS) PRINT*,'ERROR:MPI_Type_create_struct',myid
  CALL MPI_TYPE_COMMIT(particletype, ierr)
  IF(ierr.NE.MPI_SUCCESS) PRINT*,'ERROR:MPI_TYPE_COMMIT',myid

  CALL MPI_TYPE_INDEXED(nwrite_particles, blocklens, disps, particletype, particletype_indx, ierr)
  IF(ierr.NE.MPI_SUCCESS) PRINT*,'ERROR:MPI_TYPE_INDEXED',myid
  CALL MPI_TYPE_COMMIT(particletype_indx, ierr)

  ALLOCATE(write_particles(1:nwrite_particles))
  DO k = 1, nwrite_particles
     write_particles(k)%x = REAL(myid*nwrite_particles+k, KIND=dp) + .01_dp
     write_particles(k)%y = REAL(myid*nwrite_particles+k, KIND=dp) + .02_dp
     write_particles(k)%z = REAL(myid*nwrite_particles+k, KIND=dp) + .03_dp
  ENDDO

  filename = TRIM(prefix)//'.mesh'

!  CALL MPI_INFO_SET(info, "coll_write_bufsize", 24, ierr)
!  CALL MPI_INFO_SET(info, "bg_nodes_pset", "8", ierr)

  CALL MPI_File_open(MPI_COMM_WORLD, filename, MPI_MODE_RDWR + MPI_MODE_CREATE, info, fh, ierr)

  IF(ierr.NE.0) PRINT*,'ERROR:MPI_File_open',myid

  myndtot = nwrite_particles
  CALL MPI_File_set_view(fh, 0_MPI_OFFSET_KIND, particletype, particletype_indx, "native", info, ierr)

  t1 = MPI_Wtime()
  CALL MPI_File_write_at_all(fh, 0_MPI_OFFSET_KIND, write_particles, nwrite_particles, particletype, status, ierr)
  t2 = MPI_Wtime()
  xtiming(1) = t2-t1
  IF (ierr .NE. MPI_SUCCESS)THEN
     CALL MPI_Error_string(ierr, error_string, reslen, ierr)
     PRINT*,myid, TRIM(error_string)
  ENDIF

  ALLOCATE(read_particles(1:nwrite_particles))
  t1 = MPI_Wtime()
  CALL MPI_File_read_at_all(fh, 0_MPI_OFFSET_KIND, read_particles, nwrite_particles, particletype, status, ierr)
  t2 = MPI_Wtime()
  xtiming(2) = t2-t1

!!$  DO j = 1, numpr
!!$     IF(myid.EQ.j-1)THEN
!!$        DO i=1,nwrite_particles
!!$           IF(myid.eq.1) PRINT*,myid,i,read_particles(i)%x, read_particles(i)%y, read_particles(i)%z
!!$        ENDDO
!!$     ENDIF
!!$  ENDDO

  CALL MPI_File_close(fh, ierr)

  t2 = MPI_Wtime()
  xtiming(3) = t2-t0

  CALL MPI_Reduce(xtiming, timing, 3, MPI_DOUBLE, &
               MPI_SUM, 0, MPI_COMM_WORLD, mpierror)
  CALL MPI_Reduce(xtiming, timingMin, 3, MPI_DOUBLE, &
               MPI_MIN, 0, MPI_COMM_WORLD, mpierror)
  CALL MPI_Reduce(xtiming, timingMax, 3, MPI_DOUBLE, &
               MPI_MAX, 0, MPI_COMM_WORLD, mpierror)

  CALL MPI_Reduce(myndtot, nnavg, 1, MPI_INTEGER, &
               MPI_SUM, 0, MPI_COMM_WORLD, mpierror)
  CALL MPI_Reduce(myndtot, nnmin, 1, MPI_INTEGER, &
               MPI_MIN, 0, MPI_COMM_WORLD, mpierror)
  CALL MPI_Reduce(myndtot, nnmax, 1, MPI_INTEGER, &
               MPI_MAX, 0, MPI_COMM_WORLD, mpierror)

  IF(myid.EQ.0)THEN
    WRITE(*,'(i0,3(x,3(f14.7)),3(x,i0))') numpr, timing(1)/DBLE(numpr), timingMin(1), timingMax(1), &
       timing(2)/DBLE(numpr), timingMin(2), timingMax(2), &
       timing(3)/DBLE(numpr), timingMin(3), timingMax(3), &
          nnavg/numpr,nnmin,nnmax
  ENDIF

  CALL MPI_FINALIZE(ierr)

END PROGRAM PARALLELWRITE
