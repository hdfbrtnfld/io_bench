PROGRAM main

  USE ISO_C_BINDING

  IMPLICIT NONE

  CHARACTER(LEN=180) :: prefix
  INTEGER :: len_prefix
  REAL, POINTER :: tpwgts=>NULL()
  INTEGER(C_INT) :: numnp, numel

  REAL*8 :: coor_x, coor_y
  INTEGER, DIMENSION(:), ALLOCATABLE :: node_mark
  INTEGER, DIMENSION(:,:), ALLOCATABLE :: lm

  INTEGER :: iaux1, iaux2, iaux3, i, j, k, npel, ind

  INTEGER, ALLOCATABLE, DIMENSION(:) :: npart
  INTEGER(C_INT), ALLOCATABLE, DIMENSION(:) :: epart,numnppp
  INTEGER(C_INT), ALLOCATABLE, DIMENSION(:) :: eptr, eind

  INTEGER(C_INT) :: status
  INTEGER(C_INT), EXTERNAL :: METIS_SetDefaultOptions, METIS_PartMeshNodal

  INTEGER :: edgecut
  INTEGER, DIMENSION(1:14) :: nprocs = (/1,2,4,8,16,32,64,128,256,512,1024,2048,4096,8192/)
  INTEGER, POINTER                        :: vwgt=>NULL(), vsize=>NULL()
  INTEGER(c_int), DIMENSION(1:40), TARGET :: opts
  INTEGER(c_int) :: objval
  TYPE(C_PTR) :: f_ptr
  CHARACTER(LEN=5) :: ich5
  LOGICAL :: tecplot, gmsh, stream
  LOGICAL :: i2
  INTEGER :: i1, i3, iold, iseg, icnt, nmin, nmax, eltype
  REAL ::  aver, sumxx, stdev, dev
  INTEGER, DIMENSION(:), ALLOCATABLE :: sumx, i4, ncontseg,ix
  
  tecplot = .FALSE.
  gmsh = .TRUE.
  stream = .TRUE.

  prefix = 'recdia'
!
! 5.1) open node file created by triangle
!
  OPEN(22,file=TRIM(prefix)//'.1.node',status='old')
  READ(22,*) numnp,iaux1,iaux2,iaux3

!    
! 5.2) open element file created by triangle
!     

  PRINT*,'Reading elements'

  OPEN(23,file=TRIM(prefix)//'.1.ele',status='old')
  READ(23,*) numel, npel, iaux2

  ALLOCATE(eptr(1:numel+1), eind(npel*numel))

  ind = 1
  eptr(1) = ind
  DO i=1,numel
     READ(23,*) iaux1,(eind(j),j=ind,ind+npel-1)
     ind = ind + npel
     eptr(i+1) = ind
  ENDDO

  
!  GOTO 100

  ALLOCATE(epart(1:numel), npart(1:numnp))

  status = METIS_SetDefaultOptions(opts)

  opts(18) = 1 ! METIS_OPTION_NUMBERING (fortran)


  PRINT*,'Partitioning'
  DO i = 1, 3 !14
     IF(nprocs(i).NE.1)THEN
        status = METIS_PartMeshNodal( &
             numel, &
             numnp, &
             eptr, &
             eind, &
             vwgt, &
             vsize, &
             INT(nprocs(i),C_INT),&
             tpwgts,&
             opts, &
             objval,&
             epart,&
             npart )
     ELSE
        epart = 1
        npart = 1
     ENDIF
        
     WRITE(ich5,'(I5.5)') nprocs(i)

!     OPEN(24,file=TRIM(prefix)//'.1.part_'//ich5, FORM='unformatted')
     !OPEN(24,file=TRIM(prefix)//'.1.part_'//ich5, FORM='formatted')
!     WRITE(24) numel, nprocs(i)
!     DO j=1,numel
!        WRITE(24) j,epart(j)
!     ENDDO
!     CLOSE(24)

     OPEN(24,file=TRIM(prefix)//'.1.npart_'//ich5, FORM='unformatted')
     ALLOCATE(numnppp(1:nprocs(i)))

     
     ALLOCATE(ncontseg(1:numnp) )

     icnt = 0
     numnppp = 0

     iold = npart(1)
     iseg = 1
     icnt = 1
     DO j = 1, numnp

        IF(npart(j).NE.iold)THEN
           iold = npart(j)
           ncontseg(icnt) = iseg
           iseg = 1
           icnt = icnt + 1
           IF(icnt.GT.numnp)THEN
              STOP 'ERROR: allocated size of ncontseg not large enough'
           ENDIF
        ELSE
           iseg = iseg + 1
        ENDIF
        
        numnppp(npart(j)) = numnppp(npart(j)) + 1

     ENDDO

     IF(icnt.EQ.1)THEN
        ncontseg(icnt) = 1
     ELSE
        ncontseg(icnt) = iseg
     ENDIF

     
     aver = 0.0
     sumxx = 0.0

     nmin = HUGE(1)
     nmax = 0

     DO j = 1, icnt
        
        nmax = MAX(nmax,ncontseg(j))
        nmin = MIN(nmin,ncontseg(j))

        dev = REAL(ncontseg(j)) - aver
        aver = aver + dev/REAL(j)
        sumxx = sumxx + dev*(REAL(ncontseg(j))-aver)
     END DO
     stdev = SQRT( sumxx / REAL(icnt-1) ) 


     PRINT*,nprocs(i),'average',aver, stdev, nmin, nmax

     DO j = 1, nprocs(i)
        WRITE(24) j-1, numnppp(j)
        DO k = 1, numnp
           IF(npart(k).EQ.j)THEN
              WRITE(24) k
           ENDIF
        ENDDO
     ENDDO
     CLOSE(24)


     OPEN(30,file=TRIM(prefix)//'.dist'//ich5, form='unformatted', ACCESS="STREAM")

     DO j = 1, icnt
        WRITE(30) ncontseg(j)
     ENDDO

     CLOSE(30)

     DEALLOCATE(numnppp, ncontseg)

  ENDDO

!100 CONTINUE

  IF(gmsh) OPEN(25,file=trim(prefix)//'.gmsh')
  IF(tecplot) OPEN(26,file=TRIM(prefix)//'.tec')
  IF(stream) OPEN(UNIT=27, FILE=TRIM(prefix)//'.mesh', FORM="unformatted", STATUS="UNKNOWN", ACCESS="STREAM")

  IF(gmsh)THEN
     WRITE(25,'("$MeshFormat")')
     WRITE(25,'("2.1 0 3")')
     WRITE(25,'("$EndMeshFormat")')
  ENDIF

  IF(tecplot)THEN
     WRITE(26,'("title = ""TRIANGLE MESH""")')
     WRITE(26,'( ("variables = ""x"",""y"",""z""") )')
     WRITE(26,'( "zone n=",I0,", e=",I0,", f=fepoint, et=triangle")') numnp,numel
  ENDIF

  IF(gmsh)THEN
     WRITE(25,'("$Nodes")')
     WRITE(25,*) numnp
  ENDIF
  DO i = 1, numnp
     READ(22,*) iaux1, coor_x, coor_y
     IF(gmsh)WRITE(25,'(i0,1x,f15.4,1x,f15.4,1x,f15.4)') i, coor_x, coor_y, 0.d0
     IF(tecplot) WRITE(26,'(f15.4,1x,f15.4)') coor_x, coor_y, 0.d0
     IF(stream) WRITE(27) coor_x, coor_y, 0.d0
  ENDDO
  CLOSE(22)

  WRITE(25,'("$EndNodes")')

  IF(gmsh)THEN
     WRITE(25,'("$Elements")')
     WRITE(25,*) numel
  ENDIF

  IF(npel.EQ.3)THEN
     eltype = 2
  ELSE IF(npel.EQ.6)THEN
     eltype = 9
  ENDIF

  DO i=1,numel
     IF(gmsh) WRITE(25,*) i, eltype, 1, 99, eind(eptr(i):eptr(i)+npel-1)
     IF(tecplot) WRITE(26,*) eind(eptr(i):eptr(i)+npel-1) 
     IF(stream) WRITE(27) eind(eptr(i):eptr(i)+npel-1)
  ENDDO

  IF(gmsh) THEN
     WRITE(25,'("$EndElements")')
     CLOSE(25)
  ENDIF
  IF(tecplot) CLOSE(26)
  IF(stream) CLOSE(27)

  PRINT*,'mbconvert'

  IF(gmsh) CALL EXECUTE_COMMAND_LINE("./mbconvert -f H5M "//TRIM(prefix)//".gmsh "//TRIM(prefix)//".h5m")

END PROGRAM main
