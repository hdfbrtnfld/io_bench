
PROGRAM main

  IMPLICIT NONE

!!$  0  H5M (0.00 s) Getting file summary
!!$  0  H5M (0.20 s) Communicating file summary
!!$  0  H5M (0.20 s) Opening "bglockless:Meshes/64bricks_32mtet_rib_1.h5m" for parallel IO
!!$  0  H5M (0.23 s) RETRIEVING TAGGED ENTITIES
!!$  0  H5M (0.25 s) GATHERING ADDITIONAL ENTITIES
!!$  0  H5M (0.25 s)   doing read_set_ids_recursive
!!$  0  H5M (0.26 s)   doing get_set_contents
!!$  0  H5M (0.27 s) GATHERING NODE IDS
!!$  0  H5M (0.27 s) READING Edge2 CONNECTIVITY (0 elems in 0 selects)
!!$  0  H5M (0.27 s) READING Tet4 CONNECTIVITY (33061416 elems in 1 selects)
!!$  0  H5M (62.56 s) READING Tri3 CONNECTIVITY (0 elems in 0 selects)
!!$  0  H5M (62.56 s) READING NODE COORDINATES (5602195 nodes in 1 selects)
!!$  0  H5M (63.91 s) READING ELEMENTS
!!$  0  H5M (65.93 s) UPDATING CONNECTIVITY ARRAYS FOR READ ELEMENTS
!!$  0  H5M (80.29 s) READING ADJACENCIES
!!$  0  H5M (80.29 s) CHECKING FOR AND DELETING NON-SIDE ELEMENTS
!!$  0  H5M (524.57 s) READING SETS
!!$  0  H5M (524.57 s)   doing find_sets_containing
!!$  0  H5M (524.58 s)   doing read_sets
!!$  0  H5M (524.60 s) READING TAGS
!!$  0  H5M (526.41 s) PARTIAL READ COMPLETE.
!!$  0  H5M (526.41 s) Reading QA records
!!$  0  H5M (526.42 s) Cleaning up
!!$  0  H5M (526.42 s) Read finished.

  INTEGER, PARAMETER :: nsize = 20

  REAL, DIMENSION(:,:), ALLOCATABLE :: DATA
  REAL, DIMENSION(:), ALLOCATABLE :: average
  CHARACTER(LEN=180) :: filename
  CHARACTER(LEN=180) :: input
  INTEGER :: arg, nstart
  character(LEN=20) :: arg_chr
  INTEGER :: i, j, k, kk, n1,n2,n3,n4, n, ios, id, nio
  LOGICAL :: file_exist

  CALL execute_command_line ("\ls *.error | wc -l > scratch", exitstat=i)
  CALL execute_command_line ("\ls *.error >> scratch", exitstat=i)

  OPEN(9,file='scratch')
  READ(9,*) kk

  OPEN(11,file="summary.dat",form='formatted')

  DO k = 1, kk

     
     READ(9,'(A)') filename

     PRINT*,'Reading ', TRIM(filename)

     OPEN(10,file=filename,form='formatted',iostat=ios)
     
     n = 0
     outer: DO 
        READ(10,'(A)',iostat=ios) input
        IF(ios.LT.0) EXIT
        n2 = INDEX(input,'.h5m')
        IF(n2.NE.0)THEN
           DO j = 1,10
             IF(input(n2-j:n2-j).eq.'_')THEN
                READ(input(n2-j+1:n2-1), '(I10)' ) n
                EXIT outer
             ENDIF 
           ENDDO
        ENDIF
     ENDDO outer

     IF(n.EQ.0) STOP '...Failed to find num_parts'

     ALLOCATE(DATA(1:nsize,0:n-1))
     ALLOCATE(average(1:8*3))

     data = 0.d0

     REWIND(10)

     nio = 0

     DO
10      CONTINUE
        READ(10,'(A)',iostat=ios) input
        IF(ios.LT.0) EXIT
        n1 = INDEX(input,'H5M')
        IF(n1.NE.6) GOTO 10

        READ(input(1:n1-1), '(I10)' ) id


        n2 = INDEX(input,'READING Tet4 CONNECTIVITY')
        IF(n2.NE.0)THEN
           n2 = INDEX(input,'s)')
           READ(input(n1+5:n2-1),'(F8.2)') DATA(1,id) 
           GOTO 10
        ELSE
           n2 = INDEX(input,'READING Tri3 CONNECTIVITY')
           IF(n2.NE.0)THEN
              n2 = INDEX(input,'s)')
              READ(input(n1+5:n2-1),'(F8.2)') DATA(1,id) ! GATHERING NODE IDS
              GOTO 10
           ENDIF
        ENDIF
        
        n2 = INDEX(input,'READING NODE COORDINATES') 
        IF(n2.NE.0)THEN
           nio = nio + 1
           n2 = INDEX(input,'s)')
           READ(input(n1+5:n2-1),'(F8.2)') DATA(2,id) ! READING Tet4 CONNECTIVITY
           GOTO 10
        ENDIF
        n2 = INDEX(input,'READING ELEMENTS') 
        IF(n2.NE.0)THEN
           n2 = INDEX(input,'s)')
           READ(input(n1+5:n2-1),'(F8.2)') DATA(3,id) ! READING NODE COORDINATES
           GOTO 10
        ENDIF
        n2 = INDEX(input,'UPDATING CONNECTIVITY ARRAYS FOR READ ELEMENTS') 
        IF(n2.NE.0)THEN
           n2 = INDEX(input,'s)')
           READ(input(n1+5:n2-1),'(F8.2)') DATA(4,id) ! READING ELEMENTS
           GOTO 10
        ENDIF
        n2 = INDEX(input,'CHECKING FOR AND DELETING NON-SIDE ELEMENTS') 
        IF(n2.NE.0)THEN
           n2 = INDEX(input,'s)')
           READ(input(n1+5:n2-1),'(F8.2)') DATA(5,id) ! UPDATING CONNECTIVITY ARRAYS FOR READ ELEMENTS 
           GOTO 10
        ENDIF
        n2 = INDEX(input,'READING SETS') 
        IF(n2.NE.0)THEN
           n2 = INDEX(input,'s)')
           READ(input(n1+5:n2-1),'(F8.2)') DATA(6,id) ! CHECKING FOR AND DELETING NON-SIDE ELEMENTS 
           GOTO 10
        ENDIF
        n2 = INDEX(input,'READING TAGS')
        IF(n2.NE.0)THEN
           n2 = INDEX(input,'s)')
           READ(input(n1+5:n2-1),'(F8.2)') DATA(7,id) ! READING SETS
           GOTO 10
        ENDIF
        n2 = INDEX(input,'PARTIAL READ COMPLETE')
        IF(n2.NE.0)THEN
           n2 = INDEX(input,'s)')
           READ(input(n1+5:n2-1),'(F8.2)') DATA(8,id) ! READING TAGS
           GOTO 10
        ENDIF
     ENDDO
     
!!$     DO i = 0,n-1
!!$        PRINT*,DATA(1:8,i)
!!$     ENDDO


     i=1
     DO j = 1, 8
     !   PRINT*,j,DATA(j,0:n-1),n ! SUM(DATA(j,0:n-1))/REAL(n)
        average(i) = SUM(DATA(j,0:nio-1))/REAL(nio)
        average(i+1) = MINVAL(DATA(j,0:n-1))
        average(i+2) = MAXVAL(DATA(j,0:n-1))
        i = i + 3
     ENDDO

     WRITE(11,'(I0,100(1X,F8.2))') n, average(1),average(2),average(3), & ! 2,GATHERING NODE IDS (everthing up to)
          ABS(average(4)-average(1)),average(5)-average(2),average(6)-average(3), & ! 5,READING Tet4 CONNECTIVITY
          ABS(average(7)-average(4)),average(8)-average(5),average(9)-average(6), & ! 8,READING NODE COORDINATES
          ABS(average(10)-average(7)),average(11)-average(8),average(12)-average(9), &  ! 11,READING ELEMENTS
          ABS(average(13)-average(10)),average(14)-average(11),average(15)-average(12), & ! 14,UPDATING CONNECTIVITY ARRAYS FOR READ ELEMENTS
          ABS(average(16)-average(13)),average(15)-average(14),average(18)-average(15), & ! 17,CHECKING FOR AND DELETING NON-SIDE ELEMENTS
          ABS(average(19)-average(16)),average(20)-average(17),average(21)-average(18), & ! 20,READING SETS
          ABS(average(22)-average(19)),average(23)-average(20),average(24)-average(21) ! 23,READING TAGS
         

     CLOSE(10)
     arg = arg +1


     DEALLOCATE(DATA,average)

  ENDDO

  CLOSE(11)


END PROGRAM main



