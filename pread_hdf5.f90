PROGRAM ParallelRead

  USE ISO_C_BINDING ! Enable Fortran 2003 C bindings
  USE HDF5          ! This module contains all necessary modules
  USE MPI

  IMPLICIT NONE


  INTEGER, PARAMETER :: dp = KIND(1.d0)

!  INCLUDE 'mpif.h'
  CHARACTER(LEN=6), PARAMETER :: prefix = "recdia"
  CHARACTER(LEN=23), PARAMETER :: dsetname = "/tstt/nodes/coordinates" ! Dataset name
  CHARACTER(LEN=32), PARAMETER :: eldsetname = "/tstt/elements/Tri6/connectivity" ! Dataset name
  !CHARACTER(LEN=180), PARAMETER :: filename = "bglockless:"//prefix//".h5m" ! File name
  CHARACTER(LEN=180), PARAMETER :: filename = prefix//".h5m" ! File name
  INTEGER, PARAMETER :: nrank = 2 ! Dataset rank

  INTEGER(HID_T) :: file_id,tid1       ! File identifier
  INTEGER(HID_T) :: dset_id       ! Dataset identifier
  INTEGER(HID_T) :: filespace, memspace  ! Dataspace identifier in file
  INTEGER(HID_T) :: dspace
  INTEGER(HID_T) :: plist_id,dataxfer_plist_id      ! Property list identifier
  
  INTEGER(HSIZE_T), DIMENSION(1:nrank) :: dimsf, dims, maxdims ! Dataset dimensions.
  REAL(C_DOUBLE), ALLOCATABLE, DIMENSION(:,:), TARGET :: coor
  INTEGER(HSIZE_T), ALLOCATABLE, DIMENSION(:,:) :: coord
  INTEGER, ALLOCATABLE, DIMENSION(:,:), TARGET :: ielem

  INTEGER, ALLOCATABLE, DIMENSION(:) :: epart
  INTEGER, ALLOCATABLE, DIMENSION(:) :: mynodes

  INTEGER :: nn,ne,nnpe, myndtot, iaux2

  INTEGER :: error  ! Error flags
  INTEGER :: i, j, iaux, nnmax,nnmin, nnavg

  CHARACTER(LEN=5) :: ich5
  !
  ! MPI definitions and calls.
  !
  INTEGER :: mpierror       ! MPI error flag
  INTEGER :: comm, info
  INTEGER :: numpr, myid
  REAL(KIND=dp) t1, t2, tread, treadp, treadmin, treadmax, tread1, tread2
  REAL(KIND=dp), DIMENSION(1:3) :: xtiming, timing, timingMin, timingMax

  TYPE(C_PTR) :: f_ptr
  LOGICAL, PARAMETER :: debug = .TRUE.

  REAL :: start, gettime

  TYPE(H5D_rw_multi_t), ALLOCATABLE, DIMENSION(:) :: info_md

  INTEGER(SIZE_T) :: count

  LOGICAL :: multi
  

  comm = MPI_COMM_WORLD
  info = MPI_INFO_NULL
  CALL MPI_INIT(mpierror)
  CALL MPI_COMM_SIZE(comm, numpr, mpierror)
  CALL MPI_COMM_RANK(comm, myid, mpierror)

  CALL MPI_BARRIER(comm, mpierror)
  t1 = MPI_Wtime()

  multi = .false.


  !
  ! Initialize FORTRAN interface
  !
  CALL h5open_f(error)
  !
  ! Setup file access property list with parallel I/O access.
  !
  CALL h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, error)
  CALL h5pset_fapl_mpio_f(plist_id, comm, info, error)
  call H5Pset_sieve_buf_size_f(plist_id, INT(4*1024*1024,size_t), error)

  !
  ! Open the file collectively.
  !
  CALL h5fopen_f(filename, H5F_ACC_RDONLY_F, file_id, error, access_prp = plist_id)
  
  CALL h5pclose_f(plist_id, error)

  ! READ ELEMENT CONNECTIVITY

  CALL h5dopen_f(file_id, eldsetname, dset_id, error)

  CALL h5dget_space_f(dset_id, dspace, error)

  CALL h5sget_simple_extent_dims_f(dspace, dimsf, maxdims, error)

  !
  ! Read the dataset collectively.
  !

  nnpe = dimsf(1) ! number of nodes per element
  ne  = dimsf(2) ! number of elements

  CALL h5pcreate_f(H5P_DATASET_XFER_F, dataxfer_plist_id, error)
!  CALL H5Pset_dxpl_mpio_f(dataxfer_plist_id, H5FD_MPIO_INDEPENDENT_F, error)
  CALL H5Pset_dxpl_mpio_f(dataxfer_plist_id, H5FD_MPIO_COLLECTIVE_F, error)

  ! 
  ! Close resources.
  !
  CALL h5sclose_f(dspace, error)
  CALL h5dclose_f(dset_id, error)

  CALL MPI_BARRIER(comm, mpierror)
  t2 = MPI_Wtime()

  treadp = t2-t1

  ! READ NODES

!  t1 = MPI_Wtime()

  CALL h5dopen_f(file_id, dsetname, dset_id, error)

  CALL H5Dget_space_f(dset_id, dspace, error);

  CALL h5sget_simple_extent_dims_f(dspace, dimsf, maxdims, error)

  nn = dimsf(2)

  ! READ IN PARTIONING
  
  WRITE(ich5,'(I5.5)') numpr

  OPEN(24,file=prefix//'.1.npart_'//ich5, form='unformatted')

  DO j = 1, numpr
     READ(24) iaux, myndtot
     IF(iaux.EQ.myid)THEN
        ALLOCATE(mynodes(1:myndtot))
        DO i = 1,myndtot
           READ(24) iaux2
           mynodes(i) = iaux2
        ENDDO
        EXIT
     ELSE
        DO i = 1,myndtot
           READ(24) iaux
        ENDDO
     ENDIF
  ENDDO

  ALLOCATE(coord(1:2,1:3*myndtot))

  iaux = 1
  DO i = 1,myndtot
     coord(1, iaux) = 1
     coord(2, iaux) = mynodes(i)
     coord(1, iaux+1) = 2
     coord(2, iaux+1) = mynodes(i)
     coord(1, iaux+2) = 3
     coord(2, iaux+2) = mynodes(i)
     iaux = iaux + 3
  ENDDO

  DEALLOCATE(mynodes)

  CALL MPI_BARRIER(comm, mpierror)

  t1 = MPI_Wtime()

  CALL h5screate_simple_f(1, (/INT(3*myndtot,hsize_t)/), memspace, error)
  t2 = MPI_Wtime()
  xtiming(1) = t2-t1

  t1 = MPI_Wtime()
  CALL h5sselect_elements_f(dspace, H5S_SELECT_SET_F, 2, INT(3*myndtot,size_t),& 
       coord, error)
  t2 = MPI_Wtime()
  xtiming(2) = t2-t1

  DEALLOCATE(coord)

  !
  ! Read the dataset collectively.
  !

  ALLOCATE(coor(1:3,1:myndtot))
  f_ptr = C_LOC(coor(1,1))

!  CALL MPI_BARRIER(comm, mpierror)
!  t2 = MPI_Wtime()
!  treadp = t2-t1

 ! IF(myid.EQ.0) PRINT*," Reading partitioning etc.. ", treadp

  CALL MPI_BARRIER(comm, mpierror)

  CALL h5tcopy_f(H5T_IEEE_F64BE, tid1, error)
!  CALL h5tcopy_f(H5T_IEEE_F64LE, tid1, error)

  t1 = MPI_Wtime()

  IF(.NOT.multi)THEN
     CALL h5dread_f(dset_id, tid1, f_ptr, error, mem_space_id=memspace, file_space_id=dspace,&
          xfer_prp=dataxfer_plist_id )

  ELSE

     count = 1
  
     ALLOCATE(info_md(1:1))
     
     info_md(1)%dset_id = dset_id
     info_md(1)%dset_space_id = dspace
     info_md(1)%mem_type_id = tid1 
     info_md(1)%mem_space_id = memspace
     info_md(1)%rwbuf = f_ptr
     
     CALL H5Dread_multi_f(file_id, dataxfer_plist_id, count,  info_md, error)
     
  ENDIF

  t2 = MPI_Wtime()
  xtiming(3) = t2-t1

!  PRINT*, myid,"Elapsed time is ", timing(3)

  IF(debug)THEN
     IF(myid.EQ.0)THEN
        
        WRITE(*, '(/,I0,x,A,":")') myid, dsetname
        DO i=1, myndtot
           WRITE(*,'(" [")', ADVANCE='NO')
           WRITE(*,'(80f14.7)', ADVANCE='NO') coor(1:3,i)
           WRITE(*,'(" ]")')
        ENDDO
        WRITE(*, '(/)')
     ENDIF
  ENDIF

  !
  ! Deallocate data buffer.
  !

  DEALLOCATE(coor)

  ! 
  ! Close resources.
  !
  CALL h5pclose_f(dataxfer_plist_id, error)
  CALL h5sclose_f(memspace, error)
  CALL h5sclose_f(dspace, error)
  CALL h5dclose_f(dset_id, error)
  CALL h5fclose_f(file_id, error)

  !
  ! Close FORTRAN interface
  !
  CALL h5close_f(error)
  !PRINT*,'Reducing'
  CALL MPI_Reduce(xtiming, timing, 3, MPI_DOUBLE, &
               MPI_SUM, 0, comm, mpierror)
  CALL MPI_Reduce(xtiming, timingMin, 3, MPI_DOUBLE, &
               MPI_MIN, 0, comm, mpierror)
  CALL MPI_Reduce(xtiming, timingMax, 3, MPI_DOUBLE, &
               MPI_MAX, 0, comm, mpierror)

  CALL MPI_Reduce(myndtot, nnavg, 1, MPI_INTEGER, &
               MPI_SUM, 0, comm, mpierror)
  CALL MPI_Reduce(myndtot, nnmin, 1, MPI_INTEGER, &
               MPI_MIN, 0, comm, mpierror)
  CALL MPI_Reduce(myndtot, nnmax, 1, MPI_INTEGER, &
               MPI_MAX, 0, comm, mpierror)

  IF(myid.EQ.0)THEN
    WRITE(*,'(i0,3(x,3(f14.7)),3(x,i0))') numpr, timing(1)/DBLE(numpr), timingMin(1), timingMax(1), &
       timing(2)/DBLE(numpr), timingMin(2), timingMax(2), &
       timing(3)/DBLE(numpr), timingMin(3), timingMax(3), & 
          nnavg/numpr,nnmin,nnmax
  ENDIF

  CALL MPI_FINALIZE(mpierror)
  
END PROGRAM PARALLELREAD
