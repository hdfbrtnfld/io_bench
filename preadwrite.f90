MODULE qsort_c_module

  USE HDF5

  IMPLICIT NONE
  PUBLIC :: QsortC
  PRIVATE :: Partition
  
CONTAINS

  ! Sort each partition of the elements from least to greatest
  
  RECURSIVE SUBROUTINE QsortC(A)
    INTEGER(HSIZE_T), INTENT(in out), DIMENSION(:) :: A
    INTEGER :: iq
    
    IF(SIZE(A) > 1) THEN
       CALL Partition(A, iq)
       CALL QsortC(A(:iq-1))
       CALL QsortC(A(iq:))
    ENDIF
  END SUBROUTINE QsortC
  
  SUBROUTINE Partition(A, marker)
    INTEGER(HSIZE_T), INTENT(in out), DIMENSION(:) :: A
    INTEGER, INTENT(out) :: marker
    INTEGER :: i, j
    INTEGER(HSIZE_T) :: temp
    INTEGER(HSIZE_T) :: x      ! pivot point
    x = A(1)
    i= 0
    j= SIZE(A) + 1
    
    DO
       j = j-1
       DO
          IF (A(j) <= x) EXIT
          j = j-1
       END DO
       i = i+1
       DO
          IF (A(i) >= x) EXIT
          i = i+1
       END DO
       IF (i < j) THEN
          ! exchange A(i) and A(j)
          temp = A(i)
          A(i) = A(j)
          A(j) = temp
       ELSEIF (i == j) THEN
          marker = i+1
          RETURN
       ELSE
          marker = i
          RETURN
       ENDIF
    END DO
    
  END SUBROUTINE Partition


END MODULE qsort_c_module

!************************************************************
!
!  This example shows how to read and write array datatypes
!  to a dataset.  The program first writes integers arrays
!  to a dataset with a dataspace of. It then reads back the 
!  data, and outputs it to the screen.
!
!  This file is intended for use with HDF5 Library version 1.8
!  with --enable-fortran2003 
!
!************************************************************
PROGRAM main

  USE HDF5
  USE ISO_C_BINDING
  USE MPI
  USE qsort_c_module
  
  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = KIND(1.d0)
  INTEGER, PARAMETER :: i8 = SELECTED_INT_KIND(10)
  CHARACTER(LEN=180) :: filename
  CHARACTER(LEN=180) :: filename_part
  CHARACTER(LEN=12), PARAMETER :: dataset   = "connectivity"

! Number of elements, uncomment the size wanted
!  INTEGER(KIND=i8), PARAMETER :: dim0 = 1024_i8
  INTEGER(KIND=i8), PARAMETER :: dim0 = 33554432_i8
!  INTEGER(KIND=i8), PARAMETER :: dim0 = 50331648_i8
!  INTEGER(KIND=i8), PARAMETER :: dim0 = 805306368_i8
!  INTEGER(KIND=i8), PARAMETER :: dim0 = 1610612736_i8
!  INTEGER(KIND=i8), PARAMETER :: dim0 = 3221225472_i8
!  INTEGER(KIND=i8), PARAMETER :: dim0 = 6442450944_i8

  INTEGER          , PARAMETER :: adim0     = 4
  INTEGER(HID_T)  :: file, file1, filetype, memtype, space, dset_id,dspace, filespace, memspace, dspace_g, dset_g ! Handles
  INTEGER :: hdferr
  INTEGER(HSIZE_T), DIMENSION(1:1)   :: dims = (/dim0/)
  INTEGER(HSIZE_T), DIMENSION(1:1)   :: adims = (/adim0/)
  INTEGER(HSIZE_T), DIMENSION(1:1)   :: mydims
  INTEGER(KIND=i8) :: mydim0
  INTEGER(KIND=i8), DIMENSION(:), ALLOCATABLE, TARGET :: wdata ! Write buffer 
  INTEGER(KIND=i8), DIMENSION(:), ALLOCATABLE, TARGET :: rdata ! Read buffer
  INTEGER(KIND=i8) :: i, j, k
  TYPE(C_PTR) :: f_ptr
  INTEGER(HSIZE_T), DIMENSION(1:1) :: start_g
  INTEGER(HSIZE_T), DIMENSION(1:1) :: count_g
  
  INTEGER(HSIZE_T), DIMENSION(:), ALLOCATABLE, TARGET :: coord
  INTEGER :: mpierror       ! MPI error flag
  INTEGER :: numpr, myid, AllocateStatus
  INTEGER(HID_T) :: plist_id, plist_id1, dataxfer_plist_id, h5_kind_type,h5_rkind_type
  REAL :: start, gettime
  REAL(KIND=dp) t1, t2, t3, t4, t5, t6, tread, treadp, treadmin, treadmax, tread1, tread2
  REAL(KIND=dp), DIMENSION(1:6) :: xtiming, timing, timingMin, timingMax
  CHARACTER(LEN=6) :: ich6
  CHARACTER(LEN=12) :: ich12
  LOGICAL :: rand, debug
  CHARACTER(LEN=11) :: prefix
  CHARACTER(LEN=180) :: tmp_string
  LOGICAL :: IsGPFS, hyper, status
  INTEGER :: sop, iaux, icnt
  INTEGER(HID_T) :: h5_kind_type_i ! HDF type of the specified KIND

  INTEGER :: info

  CALL MPI_INIT(mpierror)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD, numpr, mpierror)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD, myid, mpierror)

  !
  ! Initialize FORTRAN interface.
  !
  CALL h5open_f(hdferr)

! Is a gpfs file system.
!  IsGPFS = .FALSE.
  IsGPFS = .TRUE.
! Want random partitioning
!  rand = .TRUE.
  rand = .FALSE.
! degug output
  !debug = .TRUE.
  debug = .FALSE.
! want to use point selection or hyperslab selection
  hyper = .TRUE.
! hyper = .FALSE.

  prefix = ''
  IF(IsGPFS)THEN
     prefix = 'bglockless:'
     CALL MPI_Info_create(info, mpierror )
     CALL MPI_Info_set(info, "IBM_largeblock_io", "true", mpierror);
  ELSE
     info = MPI_INFO_NULL
  ENDIF

  WRITE(ich6,'(I6.6)') numpr
  filename = TRIM(prefix)//'pbench_'//ich6//'.h5'
  CALL h5pcreate_f(H5P_DATASET_XFER_F, dataxfer_plist_id, hdferr)
  CALL H5Pset_dxpl_mpio_f(dataxfer_plist_id, H5FD_MPIO_COLLECTIVE_F, hdferr)
! CALL H5Pset_dxpl_mpio_f(dataxfer_plist_id, H5FD_MPIO_INDEPENDENT_F, hdferr)


  
  ! tuning parameters
  !CALL H5Pset_buffer_f( dataxfer_plist_id, 2*2147483648_size_t, hdferr)

  h5_kind_type_i = h5kind_to_type(i8,H5_INTEGER_KIND)
  !
  ! Create array datatypes for file and memory.
  !
  CALL H5Tarray_create_f(h5_kind_type_i, 1, adims, memtype, hdferr)

  ! initialize the data

  mydim0 = dim0/numpr
  mydims = (/mydim0/)

  ALLOCATE(coord(1:mydim0), STAT=AllocateStatus)
  IF(AllocateStatus.NE.0) PRINT*,' coord, *** Not enough memory ***'

  IF(.NOT.rand)THEN
     ! just read in blocks for partitioning
     DO i = 1, mydim0
        coord(i) = myid*(mydim0) + i
     ENDDO
  ELSE

     ! read from the file generated with gen_preadwrite.f90

     h5_kind_type = h5kind_to_type(hsize_t, H5_INTEGER_KIND)

     WRITE(ich12,'(I12.12)') dim0
     filename_part = TRIM(prefix)//'gen_pbench_'//ich12//'.h5'

     CALL h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, hdferr)
     CALL h5pset_fapl_mpio_f(plist_id, MPI_COMM_WORLD, info, hdferr)

     CALL h5fopen_f(filename_part, H5F_ACC_RDONLY_F, file, hdferr, access_prp = plist_id)
     CALL h5pclose_f(plist_id, hdferr)

     CALL h5dopen_f(file, "partition", dset_g, hdferr)
     CALL h5dget_space_f(dset_g, dspace_g, hdferr)

     start_g(1)= INT(myid*mydim0, hsize_t)
     count_g(1)= INT(mydim0, hsize_t)

     CALL h5sselect_hyperslab_f(dspace_g, H5S_SELECT_SET_F, start_g, count_g, hdferr)
     
     CALL h5screate_simple_f(1, mydims, memspace, hdferr)

     f_ptr = C_LOC(coord(1))

     CALL h5dread_f(dset_g, h5_kind_type, f_ptr, hdferr, &
          mem_space_id=memspace, file_space_id=dspace_g, xfer_prp=dataxfer_plist_id)

    ! IF(debug) PRINT*,coord(1:mydim0)

     CALL H5Dclose_f (dset_g, hdferr)
     CALL H5Sclose_f (dspace_g, hdferr)
     CALL H5Sclose_f (memspace, hdferr)
     CALL H5Fclose_f (file, hdferr)

     ! order from least to greatest, this will be the case in a real table read
     CALL QsortC(coord)

     IF(myid.EQ.1.and.debug)  PRINT*,coord(1:mydim0)
   
  ENDIF

!  CALL MPI_BARRIER(MPI_COMM_WORLD, mpierror)
!  CALL MPI_FINALIZE(mpierror)

  ALLOCATE(wdata(1:mydim0*adim0), STAT=AllocateStatus)
  IF(AllocateStatus.NE.0) PRINT*,' wdata, *** Not enough memory ***'
  !
  ! Initialize data.  i is the element in the dataspace
  !

  DO i = 1,mydim0*adim0
     wdata(i) = myid*mydim0*adim0+i
  ENDDO

  ! 
  ! Set up file access property list with parallel I/O access
  !
  CALL h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, hdferr)
  CALL h5pset_fapl_mpio_f(plist_id, MPI_COMM_WORLD, info, hdferr)
  !
  ! Create a new file collectively and release property list identifier.
  !
  CALL h5fcreate_f(filename, H5F_ACC_TRUNC_F, file, hdferr, access_prp = plist_id)

  t1 = MPI_Wtime()

  !
  ! Create the dataspace for the dataset
  !
  CALL h5screate_simple_f(1, dims, filespace, hdferr)

  !
  ! Create the dataset with default properties and close filespace.
  !
  CALL H5Dcreate_f(file, dataset, memtype, filespace, dset_id, hdferr)
  CALL H5Sclose_f(filespace, hdferr)

  !
  ! Create the dataset and write the array data to it.
  !
  CALL h5screate_simple_f(1, mydims, memspace, hdferr)

  CALL H5Dget_space_f(dset_id, filespace, hdferr)

  ! use hyperslab appending

  IF(hyper)THEN

     sop = H5S_SELECT_SET_F

     start_g(1) = coord(1)-1
     count_g(1) = 1
  
     status = .FALSE.
     DO i = 2, mydim0
        IF(coord(i-1).EQ.coord(i)-1)THEN
           count_g(1) = count_g(1) + 1
           status = .TRUE.
        ELSE
           CALL h5sselect_hyperslab_f(filespace, sop, start_g, count_g, hdferr)

           status = .FALSE.
           start_g(1) = coord(i)-1
           count_g(1) = 1

           sop = H5S_SELECT_OR_F
        ENDIF
        IF(.NOT.status.AND.i.EQ.mydim0)THEN
           CALL h5sselect_hyperslab_f(filespace, sop, start_g, count_g, hdferr)
        ENDIF
        ENDDO

     IF(status)THEN
        CALL h5sselect_hyperslab_f(filespace, sop, start_g, count_g, hdferr)
     ENDIF

  ELSE

     ! use point selection

     CALL h5sselect_elements_f(filespace, H5S_SELECT_SET_F, 1, INT(mydim0,size_t), coord, hdferr)

  ENDIF

  f_ptr = C_LOC(wdata(1))

  t3 = MPI_Wtime()
  xtiming(2) = t3-t1
  CALL h5dwrite_f(dset_id, memtype, f_ptr, hdferr, &
       mem_space_id=memspace, file_space_id=filespace, xfer_prp=dataxfer_plist_id )
  t4 = MPI_Wtime()
  xtiming(3) = t4-t3

  CALL H5Dclose_f(dset_id, hdferr)
  CALL H5Sclose_f(memspace, hdferr)
  CALL H5Sclose_f(filespace, hdferr)
  t3 = MPI_Wtime()
!  CALL H5Fclose_f(file, hdferr) ! don't close file, it takes forever.
!  t2 = MPI_Wtime()
!  xtiming(3) = t2-t3

  xtiming(1) = t3-t1

  DEALLOCATE(wdata)
  !goto 20
  !
  ! Now we begin the read section.
  !
  ! Open file, dataset, and attribute.
  !
  ! Set up file access property list with parallel I/O access
  !

  t1 = MPI_Wtime()
  CALL h5dopen_f(file, dataset, dset_id, hdferr)
  !
  ! Get the datatype and its dimensions.
  !
  CALL h5dget_type_f(dset_id, filetype, hdferr)
  CALL H5Tget_array_dims_f (filetype, adims, hdferr)
  !
  ! Get dataspace and allocate memory for read buffer.  This is a
  ! three dimensional attribute when the array datatype is included.
  !
  CALL H5Dget_space_f(dset_id, filespace, hdferr)

  IF(hyper)THEN

     sop = H5S_SELECT_SET_F

     start_g(1) = coord(1)-1
     count_g(1) = 1
  
     status = .FALSE.
     DO i = 2, mydim0
        IF(coord(i-1).EQ.coord(i)-1)THEN
           count_g(1) = count_g(1) + 1
           status = .TRUE.
        ELSE
           CALL h5sselect_hyperslab_f(filespace, sop, start_g, count_g, hdferr)

           status = .FALSE.
           start_g(1) = coord(i)-1
           count_g(1) = 1

           sop = H5S_SELECT_OR_F
        ENDIF
        IF(.NOT.status.AND.i.EQ.mydim0)THEN
           CALL h5sselect_hyperslab_f(filespace, sop, start_g, count_g, hdferr)
        ENDIF
        ENDDO

     IF(status)THEN
        CALL h5sselect_hyperslab_f(filespace, sop, start_g, count_g, hdferr)
     ENDIF

  ELSE

     CALL h5sselect_elements_f(filespace, H5S_SELECT_SET_F, 1, INT(mydim0,size_t), coord, hdferr)

  ENDIF

  DEALLOCATE(coord)

  ALLOCATE(rdata(1:mydims(1)*adims(1)), STAT=AllocateStatus)
  IF(AllocateStatus.NE.0) PRINT*,' rdata, *** Not enough memory ***'
  !
  ! Read the data.
  !
  CALL h5screate_simple_f(1, mydims, memspace, hdferr)
  
  f_ptr = C_LOC(rdata(1))
  t3 = MPI_Wtime()
  xtiming(5) = t3-t1
  CALL H5Dread_f(dset_id, memtype, f_ptr, hdferr, &
       mem_space_id=memspace, file_space_id=filespace, xfer_prp=dataxfer_plist_id )
  t4 = MPI_Wtime()
  xtiming(6) = t4-t3
  !
  ! Output the data to the screen.
  !

  IF(debug)THEN
     DO i = 1, mydims(1)*adims(1)
        IF(myid.EQ.1) WRITE(*,'(80i3)') rdata(i)
     ENDDO
  ENDIF

  ! get the timing results

  xtiming(4) = t4-t1

  CALL MPI_Reduce(xtiming, timing, 6, MPI_DOUBLE, &
               MPI_SUM, 0, MPI_COMM_WORLD, mpierror)
  CALL MPI_Reduce(xtiming, timingMin, 6, MPI_DOUBLE, &
               MPI_MIN, 0, MPI_COMM_WORLD, mpierror)
  CALL MPI_Reduce(xtiming, timingMax, 6, MPI_DOUBLE, &
               MPI_MAX, 0, MPI_COMM_WORLD, mpierror)

  IF(myid.EQ.0)THEN
    WRITE(*,'(i0,10(x,3(f14.7)))') numpr, &
         timing(1)/DBLE(numpr), timingMin(1), timingMax(1), &
         timing(2)/DBLE(numpr), timingMin(2), timingMax(2), &
         timing(3)/DBLE(numpr), timingMin(3), timingMax(3), &
         timing(4)/DBLE(numpr), timingMin(4), timingMax(4), &
         timing(5)/DBLE(numpr), timingMin(5), timingMax(5), &
         timing(6)/DBLE(numpr), timingMin(6), timingMax(6)
  ENDIF

  !
  ! Close and release resources.
  !
  DEALLOCATE(rdata)
  CALL h5pclose_f(plist_id, hdferr)
  CALL H5Pclose_f(dataxfer_plist_id, hdferr)
  CALL H5Dclose_f (dset_id, hdferr)
  CALL H5Sclose_f (filespace, hdferr)
  CALL H5Sclose_f (memspace, hdferr)
  CALL H5Tclose_f (filetype, hdferr)
  CALL H5Tclose_f (memtype, hdferr)
  !t1 = MPI_Wtime()
  !CALL H5Fclose_f (file, hdferr) ! don't close it takes forever
  !t2 = MPI_Wtime()
  !print*,'H5Fclose_f', t2-t1

20 CONTINUE

  IF(IsGPFS)THEN
     CALL MPI_Info_free(info, mpierror)
  ENDIF

  CALL MPI_FINALIZE(mpierror)

END PROGRAM main
