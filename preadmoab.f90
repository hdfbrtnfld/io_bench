

!************************************************************
!
!  This example shows how to read and write array datatypes
!  to a dataset.  The program first writes integers arrays
!  to a dataset with a dataspace of. It then reads back the 
!  data, and outputs it to the screen.
!
!  This file is intended for use with HDF5 Library version 1.8
!  with --enable-fortran2003 
!
!************************************************************
PROGRAM main

  USE HDF5
  USE ISO_C_BINDING
  USE MPI
  
  IMPLICIT NONE

  INTEGER, PARAMETER :: dp = KIND(1.d0)
  INTEGER, PARAMETER :: i8 = SELECTED_INT_KIND(10)
  CHARACTER(LEN=180) :: filename
  CHARACTER(LEN=180) :: filename_part
  CHARACTER(LEN=180), PARAMETER :: dataset   = "/tstt/elements/Tet4/CONNECTIVITY"
  CHARACTER(LEN=180), PARAMETER :: datasetnda   = "/tstt/nodes/COORDINATES"
  CHARACTER(LEN=180), PARAMETER :: datasetnd   = "/tstt/nodes/coordinates"

  INTEGER(KIND=i8) :: dim0

  INTEGER(HID_T)  :: file, file1, filetype, memtype, space, dset_id,dspace, filespace, memspace, dspace_g, dset_g ! Handles
  INTEGER :: hdferr
  INTEGER(HSIZE_T), DIMENSION(1:2)   :: dims, maxdims
  INTEGER(HSIZE_T), DIMENSION(1:2)   :: adims = (/1,3/)
  INTEGER(HSIZE_T), DIMENSION(1:2)   :: mydims
  INTEGER(KIND=i8) :: mydim0, myextra, istart
  INTEGER(KIND=i8), DIMENSION(:), ALLOCATABLE, TARGET :: rdata ! Read buffer
  REAL(KIND=C_DOUBLE), DIMENSION(:), ALLOCATABLE, TARGET :: dpdata ! Read buffer
  INTEGER(KIND=i8) :: i, j, k
  TYPE(C_PTR) :: f_ptr
  INTEGER(HSIZE_T), DIMENSION(1:2) :: start_g
  INTEGER(HSIZE_T), DIMENSION(1:2) :: count_g
  
  INTEGER(HSIZE_T), DIMENSION(:), ALLOCATABLE, TARGET :: coord
  INTEGER :: mpierror       ! MPI error flag
  INTEGER :: numpr, myid, AllocateStatus
  INTEGER(HID_T) :: plist_id, plist_id1, dataxfer_plist_id, h5_kind_type,h5_rkind_type
  REAL :: start, gettime
  REAL(KIND=dp) t1, t2, t3, t4, t5, t6, tread, treadp, treadmin, treadmax, tread1, tread2
  REAL(KIND=dp), DIMENSION(1:6) :: xtiming, timing, timingMin, timingMax
  CHARACTER(LEN=6) :: ich6
  CHARACTER(LEN=12) :: ich12
  LOGICAL :: rand, debug
  CHARACTER(LEN=11) :: prefix
  CHARACTER(LEN=180) :: tmp_string
  LOGICAL :: IsGPFS, hyper, status
  INTEGER :: sop, iaux, icnt
  INTEGER(HID_T) :: h5_kind_type_i,native_dtype_id, h5_kind_type_r ! HDF type of the specified KIND

  INTEGER :: info,nhslabs
  LOGICAL :: rdconn, rdcoora, rncoor

  CALL MPI_INIT(mpierror)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD, numpr, mpierror)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD, myid, mpierror)

  rdconn = .FALSE.
  rdcoora = .TRUE.
  rncoor = .FALSE.

  !
  ! Initialize FORTRAN interface.
  !
  CALL h5open_f(hdferr)

! Is a gpfs file system.
!  IsGPFS = .FALSE.
  IsGPFS = .TRUE.
! Want random partitioning
!  rand = .TRUE.
  rand = .FALSE.
! degug output
  !debug = .TRUE.
  debug = .FALSE.
! want to use point selection or hyperslab selection
  hyper = .TRUE.
! hyper = .FALSE.

  prefix = ''
  IF(IsGPFS)THEN
     prefix = 'bglockless:'
     CALL MPI_Info_create(info, mpierror )
     CALL MPI_Info_set(info, "IBM_largeblock_io", "TRUE", mpierror);
  ELSE
     info = MPI_INFO_NULL
  ENDIF

 ! WRITE(ich6,'(I6.6)') numpr
 ! filename = TRIM(prefix)//'pbench_'//ich6//'.h5'
  WRITE(ich6,'(I6)') numpr
  filename = '64bricks_8mtet_ng_rib_1.h5m'
  filename = 'nuscale_rib_8.h5m'
  filename = 'nuscale_rib_'//TRIM(ADJUSTL(ich6))//'.h5m'
!  filename = TRIM(prefix)//'64bricks_32mtet_ng_rib_'//TRIM(ADJUSTL(ich6))//'.h5m'
  !filename = 'recdia_'//TRIM(ADJUSTL(ich6))//'.h5m'


  CALL h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, hdferr)
  CALL h5pset_fapl_mpio_f(plist_id, MPI_COMM_WORLD, info, hdferr)
  
  CALL h5pcreate_f(H5P_DATASET_XFER_F, dataxfer_plist_id, hdferr)
  CALL H5Pset_dxpl_mpio_f(dataxfer_plist_id, H5FD_MPIO_COLLECTIVE_F, hdferr)
! CALL H5Pset_dxpl_mpio_f(dataxfer_plist_id, H5FD_MPIO_INDEPENDENT_F, hdferr)

  CALL h5fopen_f(filename, H5F_ACC_RDONLY_F, file, hdferr, access_prp = plist_id)

  t1 = MPI_Wtime()
  CALL h5dopen_f(file, datasetnd, dset_id, hdferr)
     
  !
  ! Get the datatype and its dimensions.
  !
  CALL h5dget_type_f(dset_id, filetype, hdferr)
  !
  ! Get dataspace and allocate memory for read buffer.  This is a
  ! three dimensional attribute when the array datatype is included.
  !
  CALL H5Dget_space_f(dset_id, filespace, hdferr)
     
  CALL h5sget_simple_extent_dims_f(filespace, dims, maxdims, hdferr)

  WRITE(ich12,'(I12)') myid
     
  OPEN(88, file = 'h5m_part'//TRIM(ADJUSTL(ich12))//'.h5')
     
  READ(88,*) nhslabs
     
  sop = H5S_SELECT_SET_F
  start_g(1) = 0
  count_g(1) = 3
  mydims = 0
  DO i = 1, nhslabs
     READ(88,*)  start_g(2),count_g(2)
     mydims(2) = mydims(2) + count_g(2)
     CALL h5sselect_hyperslab_f(filespace, sop, start_g, count_g, hdferr)
     sop = H5S_SELECT_OR_F
  ENDDO
  mydims(1) = 3

  ALLOCATE(dpdata(1:mydims(1)*mydims(2)), STAT=AllocateStatus)
  IF(AllocateStatus.NE.0) PRINT*,' rdata, *** Not enough memory ***'
  !
  ! Read the data.
  !
  CALL h5screate_simple_f(2, mydims, memspace, hdferr)
  
  f_ptr = C_LOC(dpdata(1))
  t3 = MPI_Wtime()
  xtiming(5) = t3-t1
  CALL H5Dread_f(dset_id, H5T_IEEE_F64LE, f_ptr, hdferr, &
       mem_space_id=memspace, file_space_id=filespace, xfer_prp=dataxfer_plist_id )
  t4 = MPI_Wtime()
  xtiming(6) = t4-t3
  !
  ! Output the data to the screen.
  !
     
  IF(debug)THEN
    ! PRINT*,mydims(1)*mydims(2)
     DO i = 1, mydims(1)*mydims(2)
        IF(myid.EQ.0) WRITE(*,'(F14.7)') dpdata(i)
     ENDDO
  ENDIF

  !
  ! Close and release resources.
  !
  DEALLOCATE(dpdata)
  CALL H5Dclose_f (dset_id, hdferr)
  CALL H5Sclose_f (filespace, hdferr)
  CALL H5Sclose_f (memspace, hdferr)
  CALL H5Tclose_f (filetype, hdferr)
 

  ! get the timing results

  xtiming(4) = t4-t1
  
  CALL MPI_Reduce(xtiming, timing, 6, MPI_DOUBLE, &
       MPI_SUM, 0, MPI_COMM_WORLD, mpierror)
  CALL MPI_Reduce(xtiming, timingMin, 6, MPI_DOUBLE, &
       MPI_MIN, 0, MPI_COMM_WORLD, mpierror)
  CALL MPI_Reduce(xtiming, timingMax, 6, MPI_DOUBLE, &
       MPI_MAX, 0, MPI_COMM_WORLD, mpierror)
  
  IF(myid.EQ.0)THEN
     WRITE(*,'(i0,10(x,3(f14.7)))') numpr, &
          timing(4)/DBLE(numpr), timingMin(4), timingMax(4), &
          timing(5)/DBLE(numpr), timingMin(5), timingMax(5), &
          timing(6)/DBLE(numpr), timingMin(6), timingMax(6)
  ENDIF
     
  CALL h5pclose_f(plist_id, hdferr)
  CALL H5Pclose_f(dataxfer_plist_id, hdferr)
  !t1 = MPI_Wtime()
  CALL H5Fclose_f (file, hdferr) ! don't close it takes forever
  !t2 = MPI_Wtime()
  !print*,'H5Fclose_f', t2-t1
  
20 CONTINUE

  IF(IsGPFS)THEN
     CALL MPI_Info_free(info, mpierror)
  ENDIF
  
  CALL MPI_FINALIZE(mpierror)
  
END PROGRAM main
