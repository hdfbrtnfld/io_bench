
UNAME_N := $(shell uname -n)

HDF_DIR =~/packages/trunk/hdf5

ifeq ($(UNAME_N),jam)
#  HDF_DIR = /scr/brtnfld/hdf5_mpiio/hdf5
#  HDF_DIR = /scr/brtnfld/hdf5_trunk/hdf5
  HDF_DIR = /scr/brtnfld/hdf5_mpiio2/hdf5
  METIS = /home/brtnfld/packages/metis-5.1.0/build/Linux-i686/libmetis/
endif
ifeq (vesta,$(findstring vesta,$(UNAME_N)))
  HDF_DIR =~/packages/phdf5-trunk
  METIS = /gpfs/vesta-home/brtnfld/packages/metis-5.1.0/build/Linux-ppc64/libmetis
endif
ifeq (cetus,$(findstring cetus,$(UNAME_N)))
  HDF_DIR =~/packages/phdf5-trunk
  METIS = /gpfs/mira-home/brtnfld/packages/metis-5.1.0/build/Linux-ppc64/libmetis -L/soft/perftools/mpiP/lib -lmpiP -liberty
endif
ifeq (mira,$(findstring mira,$(UNAME_N)))
  HDF_DIR =~/packages/phdf5-trunk-new
  METIS = /gpfs/mira-home/brtnfld/packages/metis-5.1.0/build/Linux-ppc64/libmetis -L/soft/perftools/mpiP/lib -lmpiP -liberty
endif
ifeq (hopper,$(findstring hopper,$(UNAME_N)))
  HDF_DIR =/global/u1/b/brtnfld/packages/gnu/phdf5-trunk
  METIS =/global/u1/b/brtnfld/packages/gnu/metis-5.1.0/build/Linux-x86_64/libmetis
endif
ifeq (marmot,$(findstring marmot,$(UNAME_N)))
  HDF_DIR =/Users/brtnfld/packages/trunk/hdf5
  METIS =/Users/brtnfld/packages/metis-5.1.0/build/Darwin-x86_64/libmetis
endif

F90 = $(HDF_DIR)/bin/h5pfc
CC = $(HDF_DIR)/bin/h5pcc
LIB = -L$(METIS)

F90FLAGS = -O3 -g

OBJF90 = triangle2h5.o
OBJF90_p = pread_hdf5.o
OBJF90_md = pread_hdf5_md.o
OBJF90_t = ProcData.o
OBJF90_prw = preadwrite.o
OBJF90_prm = preadmoab.o
OBJCC_prw = preadwrite_c.o
OBJF90_genprw = gen_preadwrite.o
OBJF90_Insert2h5m = Insert2h5m.o

all: triangle2h5 pread_hdf5 ProcData preadwrite gen_preadwrite Insert2h5m preadmoab

triangle2h5: $(OBJF90)
	$(F90) $(F90FLAGS) $(OBJF90) -o $@ $(LIB) -lmetis

pread_hdf5: $(OBJF90_p)
	$(F90) $(F90FLAGS) $(OBJF90_p) -o $@

ProcData: $(OBJF90_t)
	$(F90) $(F90FLAGS) $(OBJF90_t) -o $@ 

preadwrite: $(OBJF90_prw)
	$(F90) $(F90FLAGS) $(OBJF90_prw) -o $@

preadmoab: $(OBJF90_prm)
	$(F90) $(F90FLAGS) $(OBJF90_prm) -o $@

#-L/soft/perftools/mpiP/lib -lmpiP -liberty

preadwrite_c: $(OBJCC_prw)
	$(CC) $(F90FLAGS) $(OBJCC_prw) -o $@ -L/soft/perftools/mpiP/lib -lmpiP -liberty

gen_preadwrite: $(OBJF90_genprw)
	$(F90) $(F90FLAGS) $(OBJF90_genprw) -o $@ 

Insert2h5m: $(OBJF90_Insert2h5m)
	$(F90) $(F90FLAGS) $(OBJF90_Insert2h5m) -o $@ 

.SUFFIXES: .o .f90

.f90.o:
	$(F90) $(F90FLAGS) -c $< -o $@ $(LIB)  -lmetis

clean:
	rm -f *.o *.mod triangle2h5 pread preadwrite gen_readwrite ProcData 

spotless:
	rm -f *.o *.mod triangle2h5 pread preadwrite gen_readwrite ProcData  *~
