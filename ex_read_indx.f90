PROGRAM struct

  USE mpi

  IMPLICIT NONE

  INTEGER, PARAMETER :: nelem = 1
  INTEGER :: numpr, myid, i, ierr, size, nread_particles
  INTEGER :: fh
  INTEGER :: status
  INTEGER, ALLOCATABLE, DIMENSION(:) :: disps
  INTEGER, ALLOCATABLE, DIMENSION(:) :: blocklens

  INTEGER(kind=MPI_ADDRESS_KIND), DIMENSION(1:nelem) :: offsets

  TYPE Particle
     SEQUENCE
     REAL*4 :: x, y, z
  END TYPE Particle

  TYPE(Particle), ALLOCATABLE, DIMENSION(:) ::  read_particles
  INTEGER, DIMENSION(1:nelem) :: realtypes, blockcounts
  INTEGER :: particletype, particletype_indx
  CHARACTER(len=MPI_MAX_ERROR_STRING) :: error_string
  INTEGER :: reslen

  CALL MPI_INIT(ierr)
  CALL MPI_COMM_RANK(MPI_COMM_WORLD, myid, ierr)
  CALL MPI_COMM_SIZE(MPI_COMM_WORLD, numpr, ierr)
  
  offsets(1:nelem) = 0
  realtypes(1:nelem) = MPI_REAL
  blockcounts(1:nelem) = 3

  CALL MPI_Type_create_struct(nelem, blockcounts, offsets, realtypes, particletype, ierr)
  IF(ierr.NE.MPI_SUCCESS) PRINT*,'ERROR:MPI_Type_create_struct',myid
  CALL MPI_TYPE_COMMIT(particletype, ierr)
  IF(ierr.NE.MPI_SUCCESS) PRINT*,'ERROR:MPI_TYPE_COMMIT',myid

  IF(myid.EQ.0)THEN
     nread_particles = 13
     ALLOCATE(disps(1:nread_particles))
     ALLOCATE(blocklens(1:nread_particles))
     blocklens = 1
     disps =  (/(I, I=0,25,2)/) 
  ELSE
     nread_particles = 12
     ALLOCATE(disps(1:nread_particles))
     ALLOCATE(blocklens(1:nread_particles))
     blocklens = 1
     disps = (/(I, I=1,25,2)/)
  ENDIF

  CALL MPI_TYPE_INDEXED(nread_particles, blocklens, disps, particletype, particletype_indx, ierr)
  IF(ierr.NE.MPI_SUCCESS) PRINT*,'ERROR:MPI_TYPE_INDEXED',myid

  CALL MPI_TYPE_COMMIT(particletype_indx, ierr)

  CALL MPI_File_open(MPI_COMM_WORLD, "ustream.demo", MPI_MODE_RDONLY, &
                  MPI_INFO_NULL, fh, ierr)

  IF(ierr.NE.0) PRINT*,'ERROR:MPI_File_open',myid

  ALLOCATE(read_particles(1:nread_particles))

  CALL MPI_File_set_view(fh, 0_MPI_OFFSET_KIND, particletype, particletype_indx, "native", MPI_INFO_NULL, ierr)

  read_particles(:)%x = -1
  read_particles(:)%y = -1
  read_particles(:)%z = -1

  CALL MPI_File_read_at_all(fh, 0_MPI_OFFSET_KIND, read_particles, nread_particles, particletype, status, ierr)

  IF (ierr .NE. MPI_SUCCESS)THEN
     CALL MPI_Error_string(ierr, error_string, reslen, ierr)
     PRINT*,myid, TRIM(error_string)
  ENDIF

  IF(myid.EQ.0)THEN
     DO i=1,nread_particles
        PRINT*,myid,read_particles(i)%x, read_particles(i)%y, read_particles(i)%z
     ENDDO
  ENDIF
  CALL MPI_BARRIER(MPI_COMM_WORLD,ierr)
  IF(myid.EQ.1)THEN
     DO i=1,nread_particles
        PRINT*,myid,read_particles(i)%x, read_particles(i)%y, read_particles(i)%z
     ENDDO
  ENDIF

  CALL MPI_File_close(fh, ierr)
  CALL MPI_FINALIZE(ierr) 
END PROGRAM struct
