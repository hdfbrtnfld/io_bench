! /scr/brtnfld/hdf5/hdf5/bin/h5pfc -O3 pread.f90
! /mnt/hdf/packages/mpich/3.1_gnu4.8.1/i686/bin/mpiexec -n 4 a.out
PROGRAM ParallelRead

  USE ISO_C_BINDING ! Enable Fortran 2003 C bindings
  USE HDF5          ! This module contains all necessary modules

  IMPLICIT NONE

  INCLUDE 'mpif.h'
  CHARACTER(LEN=23), PARAMETER :: dsetname = "/tstt/nodes/coordinates" ! Dataset name
  CHARACTER(LEN=32), PARAMETER :: eldsetname = "/tstt/elements/Tri3/connectivity" ! Dataset name
  CHARACTER(LEN=180), PARAMETER :: filename = "bglockless:rec.h5m" ! File name
  INTEGER, PARAMETER :: nrank = 2 ! Dataset rank

  INTEGER(HID_T) :: file_id,tid1       ! File identifier
  INTEGER(HID_T) :: dset_id       ! Dataset identifier
  INTEGER(HID_T) :: filespace, memspace  ! Dataspace identifier in file
  INTEGER(HID_T) :: dspace
  INTEGER(HID_T) :: plist_id,dataxfer_plist_id      ! Property list identifier
  
  INTEGER(HSIZE_T), DIMENSION(1:nrank) :: dimsf, dims, maxdims ! Dataset dimensions.
  REAL(C_DOUBLE), ALLOCATABLE, DIMENSION(:,:), TARGET :: coor
  INTEGER(HSIZE_T), ALLOCATABLE, DIMENSION(:,:) :: coord
  INTEGER, ALLOCATABLE, DIMENSION(:,:), TARGET :: ielem

  INTEGER, ALLOCATABLE, DIMENSION(:) :: epart
  LOGICAL, ALLOCATABLE, DIMENSION(:) :: mynodes

  INTEGER :: nn,ne,nnpe, myndtot

  INTEGER :: error  ! Error flags
  INTEGER :: i, j, iaux, nnmax,nnmin, nnavg

  CHARACTER(LEN=5) :: ich5
  !
  ! MPI definitions and calls.
  !
  INTEGER :: mpierror       ! MPI error flag
  INTEGER :: comm, info
  INTEGER :: mpi_size, mpi_rank
  REAL*8 t1, t2, tread, treadp, treadmin, treadmax, tread1, tread2
  REAL*8, DIMENSION(1:3) :: xtiming, timing, timingMin, timingMax

  TYPE(C_PTR) :: f_ptr
  LOGICAL, PARAMETER :: debug = .FALSE.

  REAL :: start, gettime

  comm = MPI_COMM_WORLD
  info = MPI_INFO_NULL
  CALL MPI_INIT(mpierror)
  CALL MPI_COMM_SIZE(comm, mpi_size, mpierror)
  CALL MPI_COMM_RANK(comm, mpi_rank, mpierror)

  CALL MPI_BARRIER(comm, mpierror)
  t1 = MPI_Wtime()
  !
  ! Initialize FORTRAN interface
  !
  CALL h5open_f(error)
  !
  ! Setup file access property list with parallel I/O access.
  !
  CALL h5pcreate_f(H5P_FILE_ACCESS_F, plist_id, error)
  CALL h5pset_fapl_mpio_f(plist_id, comm, info, error)
  call H5Pset_sieve_buf_size_f(plist_id, INT(4*1024*1024,size_t), error)

  !
  ! Open the file collectively.
  !
  CALL h5fopen_f(filename, H5F_ACC_RDONLY_F, file_id, error, access_prp = plist_id)
  
  CALL h5pclose_f(plist_id, error)

  ! READ ELEMENT CONNECTIVITY

  CALL h5dopen_f(file_id, eldsetname, dset_id, error)

  CALL h5dget_space_f(dset_id, dspace, error)

  CALL h5sget_simple_extent_dims_f(dspace, dimsf, maxdims, error)

  !
  ! Read the dataset collectively.
  !

  nnpe = dimsf(1) ! number of nodes per element
  ne  = dimsf(2) ! number of elements

  ALLOCATE(ielem(1:nnpe,1:ne))

  CALL h5pcreate_f(H5P_DATASET_XFER_F, dataxfer_plist_id, error)
!  CALL H5Pset_dxpl_mpio_f(dataxfer_plist_id, H5FD_MPIO_INDEPENDENT_F, error)
  CALL H5Pset_dxpl_mpio_f(dataxfer_plist_id, H5FD_MPIO_COLLECTIVE_F, error)

  f_ptr = C_LOC(ielem(1,1))
  CALL h5dread_f(dset_id, H5T_NATIVE_INTEGER, f_ptr, error, xfer_prp=dataxfer_plist_id)


  IF(debug)THEN
     WRITE(*, '(/,A,":")') eldsetname
     DO i=1, ne
        WRITE(*,'(" [")', ADVANCE='NO')
        WRITE(*,'(30(X,I0))', ADVANCE='NO') ielem(1:nnpe,i)
        WRITE(*,'(" ]")')
     ENDDO
     WRITE(*, '(/)')
  ENDIF
  
  ! 
  ! Close resources.
  !
  CALL h5sclose_f(dspace, error)
  CALL h5dclose_f(dset_id, error)

  CALL MPI_BARRIER(comm, mpierror)
  t2 = MPI_Wtime()

  treadp = t2-t1

  IF(mpi_rank.EQ.0) PRINT*," Reading Element Connectivity ", treadp

  ! READ NODES

!  t1 = MPI_Wtime()

  CALL h5dopen_f(file_id, dsetname, dset_id, error)

  CALL H5Dget_space_f(dset_id, dspace, error);

  CALL h5sget_simple_extent_dims_f(dspace, dimsf, maxdims, error)

  nn = dimsf(2)

  ! READ IN PARTIONING

  WRITE(ich5,'(I5.5)') mpi_size

  OPEN(20, file='rec.1.part_'//ich5)
  
  ALLOCATE(epart(1:ne))
  ALLOCATE(mynodes(1:nn))

  READ(20,'()')
  mynodes = .FALSE.
  myndtot = 0
  DO i = 1, ne
     READ(20,*) iaux, epart(iaux)
     IF(epart(iaux)-1.EQ.mpi_rank)THEN
        DO j = 1, nnpe
           IF(.NOT.mynodes(ielem(j,i)))THEN
              myndtot = myndtot + 1
              mynodes(ielem(j,i)) = .TRUE.
           ENDIF
        ENDDO
     ENDIF
  ENDDO

  DEALLOCATE(ielem)

  ALLOCATE(coord(1:2,1:3*myndtot))

  iaux = 1
  DO i = 1,nn
     IF(mynodes(i))THEN
        coord(1, iaux) = 1
        coord(2, iaux) = i
        coord(1, iaux+1) = 2
        coord(2, iaux+1) = i
        coord(1, iaux+2) = 3
        coord(2, iaux+2) = i
        iaux = iaux + 3
     ENDIF
  ENDDO

  DEALLOCATE(mynodes)

  CALL MPI_BARRIER(comm, mpierror)

  t1 = MPI_Wtime()

  CALL h5screate_simple_f(1, (/INT(3*myndtot,hsize_t)/), memspace, error)
  t2 = MPI_Wtime()
  xtiming(1) = t2-t1

  t1 = MPI_Wtime()
  CALL h5sselect_elements_f(dspace, H5S_SELECT_SET_F, 2, INT(3*myndtot,size_t),& 
       coord, error)
  t2 = MPI_Wtime()
  xtiming(2) = t2-t1

  DEALLOCATE(coord)

  !
  ! Read the dataset collectively.
  !

  ALLOCATE(coor(1:3,1:myndtot))
  f_ptr = C_LOC(coor(1,1))

!  CALL MPI_BARRIER(comm, mpierror)
!  t2 = MPI_Wtime()
!  treadp = t2-t1

 ! IF(mpi_rank.EQ.0) PRINT*," Reading partitioning etc.. ", treadp

  CALL MPI_BARRIER(comm, mpierror)

  PRINT*,'Start Reading'

  CALL h5tcopy_f(H5T_IEEE_F64LE, tid1, error)

  t1 = MPI_Wtime()

  CALL h5dread_f(dset_id, tid1, f_ptr, error, mem_space_id=memspace, file_space_id=dspace,&
       xfer_prp=dataxfer_plist_id )

  t2 = MPI_Wtime()

  timing(3) = t2-t1

 ! PRINT*, mpi_rank,"Elapsed time is ", treadp

  IF(debug)THEN
     IF(mpi_rank.EQ.3)THEN
        
        WRITE(*, '(/,I0,x,A,":")') mpi_rank, dsetname
        DO i=1, myndtot
           WRITE(*,'(" [")', ADVANCE='NO')
           WRITE(*,'(80f7.3)', ADVANCE='NO') coor(1:3,i)
           WRITE(*,'(" ]")')
        ENDDO
        WRITE(*, '(/)')
        
     ENDIF
  ENDIF

  !
  ! Deallocate data buffer.
  !

  DEALLOCATE(coor)

  ! 
  ! Close resources.
  !
  CALL h5pclose_f(dataxfer_plist_id, error)
  CALL h5sclose_f(memspace, error)
  CALL h5sclose_f(dspace, error)
  CALL h5dclose_f(dset_id, error)


  CALL h5fclose_f(file_id, error)

  !
  ! Close FORTRAN interface
  !
  CALL h5close_f(error)

  CALL MPI_Reduce(xtiming, timing, 3, MPI_DOUBLE, &
               MPI_SUM, 0, comm, mpierror)
  CALL MPI_Reduce(xtiming, timingMin, 3, MPI_DOUBLE, &
               MPI_MIN, 0, comm, mpierror)
  CALL MPI_Reduce(xtiming, timingMax, 3, MPI_DOUBLE, &
               MPI_MAX, 0, comm, mpierror)

  CALL MPI_Reduce(myndtot, nnavg, 1, MPI_INTEGER, &
               MPI_SUM, 0, comm, mpierror)
  CALL MPI_Reduce(myndtot, nnmin, 1, MPI_INTEGER, &
               MPI_MIN, 0, comm, mpierror)
  CALL MPI_Reduce(myndtot, nnmax, 1, MPI_INTEGER, &
               MPI_MAX, 0, comm, mpierror)

  IF(mpi_rank.EQ.0)THEN
     WRITE(*,'(i0,x,3(f14.7),3(x,i0))')mpi_size, timing(3)/DBLE(mpi_size), timingMin(3), timingMax(3), &
          nnavg/mpi_size,nnmin,nnmax
  ENDIF

  CALL MPI_FINALIZE(mpierror)
  
END PROGRAM PARALLELREAD
