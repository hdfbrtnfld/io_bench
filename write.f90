PROGRAM writeUstream
  
  USE iso_c_binding

  IMPLICIT NONE
  INTEGER :: i, ii
  REAL*4, DIMENSION(1:3,1:25) :: buf
!  INTEGER, DIMENSION(1:3,1:25) :: buf


  OPEN(UNIT=11, FILE="ustream.demo", FORM="unformatted", STATUS="UNKNOWN", ACCESS="STREAM")

  ii = 1

  DO i=1, 25
     buf(1,i) = ii
     buf(2,i) = ii+1
     buf(3,i) = ii+2
     WRITE(11) buf(1:3,i)
     ii = ii + 3
  ENDDO
  CLOSE(UNIT=11)

!!$  OPEN(UNIT=11, FILE="ustream.demo", FORM="unformatted", STATUS="UNKNOWN", ACCESS="STREAM")

!!$  ii = 1
!!$  DO i=1, 25
!!$     READ(11) buf(1:3,1)
!!$     PRINT*,buf(1:3,1)
!!$  ENDDO
  

END PROGRAM writeUstream
