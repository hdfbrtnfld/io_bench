Goal: Investigate how to efficiently parallel read irregular 
access pattern using HDF5
	* Make point selections work for collective I/O in HDF5
 
---------------------------------------------------------------

* Create 2D 3-node mesh using software Triangle
	* Creates nodal and element connectivity files


* read_triangle.f90 Triangle files
	* Reads in element connectivity table
	* Partitions the mesh using METIS_PartMeshNodal
	* Converts the triangle files into a gmsh formatted file

* Convert the gmsh file to a MOAB hdf5 file (.h5m) using mbconvert
        
* Stand-alone benchmarking codes:

     pread.f90
	* Reads the .h5m file in parallel using point selection

     gen_preadwrite.f90
       * generates random partitioning of a elements

     preadwrite.f90
       * Writes and then reads a finite element connectivity table
       * Options to use point selection or hyperslabs
       * Options for contingous access or random access
       * Note: all the parameters are hardcoded in the source files, this can be changed 
         in the future. 

NOTE: Some of the codes require METIS. gen_preadwrite.f90 and preadwrite.f90 do not, so you can just do: make gen_preadwrite and make preadwrite to build those two programs.